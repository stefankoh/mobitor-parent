package ch.mobi.mobitor.plugin.liima.service;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ServersConfigurationServiceTest {

    private ServersConfigurationService serversConfigurationService;

    @BeforeEach
    public void setup() {
        serversConfigurationService = new ServersConfigurationService(new DefaultResourceLoader());
        serversConfigurationService.setAppServersConfigFile("classpath:ch/mobi/mobitor/plugin/liima/application-servers.json");

        // act
        serversConfigurationService.initializeAmwDeployments();
        serversConfigurationService.initializeApplicationNameMappings();
    }

    @Test
    public void appServersAreNotEmpty() {
        // assert
        List<AppServerConfig> applicationServers = serversConfigurationService.getApplicationServers();
        assertThat(applicationServers).isNotEmpty();
    }

    @Test
    public void appServersHaveAtLeastOneApplication() {
        // assert
        List<AppServerConfig> applicationServers = serversConfigurationService.getApplicationServers();
        for (AppServerConfig applicationServer : applicationServers) {
            assertThat(applicationServer).isNotNull();

            List<String> applicationNames = applicationServer.getApplicationNames();
            assertThat(applicationNames).isNotEmpty();
        }
    }
}
