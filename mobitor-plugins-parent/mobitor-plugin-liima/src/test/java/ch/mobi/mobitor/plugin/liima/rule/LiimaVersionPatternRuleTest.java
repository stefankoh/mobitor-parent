package ch.mobi.mobitor.plugin.liima.rule;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

public class LiimaVersionPatternRuleTest extends PipelineRuleTest<LiimaVersionPatternRule> {

    @Test
    public void evaluateRuleWithValidVersions() {
        // arrange
        Pipeline pipeline = createPipeline();

        LiimaInformation liimaInfo = new LiimaInformation(SERVER_NAME, APP_NAME, ENV);
        liimaInfo.setVersion("1.2.3");

        pipeline.addInformation(ENV, APP_NAME, liimaInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors()).isFalse();
    }

    @Test
    public void testRuleViolated() {
        // arrange
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        LiimaVersionPatternRule rule = new LiimaVersionPatternRule(serversConfigurationService);

        // act

        // assert
        // no violations of the version format
        assertFalse(rule.ruleViolated("1.0.0"));
        assertFalse(rule.ruleViolated("1.2.3"));
        assertFalse(rule.ruleViolated("100.21.332"));
        assertFalse(rule.ruleViolated("0.0.1"));
        assertFalse(rule.ruleViolated("0.0.0"));
        assertFalse(rule.ruleViolated("10.0.0"));

        // version format is invalid, violates the rule
        assertTrue(rule.ruleViolated("00.0.0"));
        assertTrue(rule.ruleViolated("0.00.0"));
        assertTrue(rule.ruleViolated("0.0.00"));
        assertTrue(rule.ruleViolated("0.0.01"));
        assertTrue(rule.ruleViolated("1.02.3"));
        assertTrue(rule.ruleViolated("01.02.03"));
        assertTrue(rule.ruleViolated("1.2.3-4"));
        assertTrue(rule.ruleViolated("a.b.c"));

        assertTrue(rule.ruleViolated(null));
        assertTrue(rule.ruleViolated(""));
        assertTrue(rule.ruleViolated("  "));
    }

    @Test
    public void validatesType() {
        // arrange
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        LiimaVersionPatternRule rule = new LiimaVersionPatternRule(serversConfigurationService);

        // act

        // assert
        assertTrue(rule.validatesType(LIIMA));
    }

    @Override
    protected LiimaVersionPatternRule createNewRule() {
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);

        return new LiimaVersionPatternRule(serversConfigurationService);
    }

}
