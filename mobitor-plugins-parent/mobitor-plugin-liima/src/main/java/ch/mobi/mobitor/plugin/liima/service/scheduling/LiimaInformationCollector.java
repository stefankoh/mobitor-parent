package ch.mobi.mobitor.plugin.liima.service.scheduling;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.domain.deployment.DeploymentApplication;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.liima.LiimaDeploymentsPlugin;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import ch.mobi.mobitor.service.DeploymentInformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
@ConditionalOnBean(LiimaDeploymentsPlugin.class)
public class LiimaInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(LiimaInformationCollector.class);

    public static final String CACHE_NAME_LIIMA_DEPLOYMENTS = "cache_liimaDeployments";

    private final DeploymentInformationService deploymentInformationService;
    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final CollectorMetricService collectorMetricService;

    @Autowired
    public LiimaInformationCollector(RuleService ruleService,
                                     ScreensModel screensModel,
                                     DeploymentInformationService deploymentInformationService,
                                     CollectorMetricService collectorMetricService) {
        this.ruleService = ruleService;
        this.screensModel = screensModel;
        this.deploymentInformationService = deploymentInformationService;
        this.collectorMetricService = collectorMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.liimaInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.first}")
    @CacheEvict(cacheNames = CACHE_NAME_LIIMA_DEPLOYMENTS, allEntries = true)
    public void collectLiimaInformationForScreens() {
        LOG.info("start reading Liima information...");
        long startTime = System.currentTimeMillis();

        try {
            this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateAmwInformation);
        } catch (Exception ex) {
            LOG.error("reading Liima information for screens crashed!", ex);
        }

        long stopTime = System.currentTimeMillis();
        long duration = stopTime - startTime;
        LOG.info("reading Liima information took: " + duration + "ms");

        collectorMetricService.submitCollectorDuration("liima", duration);
        collectorMetricService.updateLastRunCompleted(LIIMA);
    }

    private void populateAmwInformation(Screen screen) {
        LOG.info("reading Liima information for screen: " + screen.getConfigKey());
        Set<String> allEnvironments = new HashSet<>(screen.getEnvironments());

        // load amwDeployments per server for all environments, then filter out the ones on the current screen to keep the number of requests low (and use cache by server):
//        List<PipelineServer> pipelineServers = screen.getPipelineServers();
//        Set<String> allServerNames = pipelineServers.stream().map(PipelineServer::getServerName).collect(Collectors.toSet());
        Set<String> allServerNames = screen.getPipelines().stream().map(Pipeline::getAppServerName).collect(Collectors.toSet());

        for (String serverName : allServerNames) {
            List<Deployment> liimaDeploymentsForServer = deploymentInformationService.retrieveDeployments(serverName);
            if (isEmpty(liimaDeploymentsForServer)) {
                // TODO why can this happen?!
                liimaDeploymentsForServer = new ArrayList<>();
            }
            List<Deployment> filteredAmwDeployment = deploymentInformationService.filterDeployments(liimaDeploymentsForServer, allEnvironments);

            for (Deployment deployment : filteredAmwDeployment) {
                for (DeploymentApplication appWithVersion : deployment.getApplications()) {
                    String amwApplicationName = appWithVersion.getApplicationName();

                    List<LiimaInformation> matchingInformationList = screen.getMatchingInformation(LIIMA, deployment.getEnvironment(), deployment.getServerName(), amwApplicationName);
                    // usually there will be one LiimaInformation:
                    if (matchingInformationList.size() > 1) {
                        LOG.warn("There is a ServerContext that as multiple LiimaInformation elements for one environment:" + amwApplicationName + " Env:" + deployment.getEnvironment());
                    }
                    // update LiimaInformation with deployment information:
                    updateLiimaInformation(matchingInformationList, deployment);
                }
            }
        }

        LOG.info("reading of Liima information complete for screen: " + screen.getConfigKey());

        screen.setRefreshDate(LIIMA, new Date());
        ruleService.updateRuleEvaluation(screen, LIIMA);
    }

    private void updateLiimaInformation(List<LiimaInformation> liimaInformationList, Deployment deployment) {
        Long deploymentDate = deployment.getDeploymentDate();
        String state = deployment.getState();
        String trackingId = deployment.getId();

        for (LiimaInformation liimaInfo : liimaInformationList) {
            liimaInfo.setVersion(getVersion(deployment, liimaInfo.getApplicationName()));
            liimaInfo.setDeploymentDate(deploymentDate);
            liimaInfo.setState(state);
            liimaInfo.setTrackingId(trackingId);

            updateLiimaDeploymentWebUri(liimaInfo);
        }
    }

    private void updateLiimaDeploymentWebUri(LiimaInformation liimaInfo) {
        if (isBlank(liimaInfo.getLiimaDeploymentWebUri())) {
            String deploymentWebUri = deploymentInformationService.createDeploymentWebUri(liimaInfo.getServerName(), liimaInfo.getApplicationName(), liimaInfo.getEnvironment());
            liimaInfo.setLiimaDeploymentWebUri(deploymentWebUri);
        }
    }

    private String getVersion(Deployment deployment, String applicationName) {
        for (DeploymentApplication appWithVersion : deployment.getApplications()) {
            if (applicationName.equals(appWithVersion.getApplicationName())) {
                return appWithVersion.getVersion();
            }
        }
        return null;
    }

}
