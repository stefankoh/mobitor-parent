package ch.mobi.mobitor.plugin.liima.rule;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;

@Component
public class AmwDeploymentRule implements PipelineRule {

    private final Set<String> failureStates = new HashSet<>(Arrays.asList("failed", "rejected"));

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation re) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<LiimaInformation> deployInfos = serverContext.getMatchingInformation(LIIMA);

            deployInfos.stream().filter(this::ruleViolated).forEach(liimaInfo -> re.addViolation(env, liimaInfo, RuleViolationSeverity.ERROR, "Deployment failed: " + liimaInfo.getServerName()));
        }
    }

    private boolean ruleViolated(LiimaInformation liimaInformation) {
        return failureStates.contains(liimaInformation.getState());
    }

    @Override
    public boolean validatesType(String type) {
        return LIIMA.equals(type);
    }

}
