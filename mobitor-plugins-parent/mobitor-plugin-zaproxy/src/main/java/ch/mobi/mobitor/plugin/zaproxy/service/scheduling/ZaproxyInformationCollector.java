package ch.mobi.mobitor.plugin.zaproxy.service.scheduling;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.Date;
import java.util.List;

import ch.mobi.mobitor.plugin.zaproxy.ZaproxyPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import ch.mobi.mobitor.plugin.zaproxy.service.client.ZaproxyProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;

import static ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation.ZAPROXY;

@Component
@ConditionalOnBean(ZaproxyPlugin.class)
public class ZaproxyInformationCollector {

    private static final Logger LOG = LoggerFactory.getLogger(ZaproxyInformationCollector.class);

    private final ZaproxyProviderService zaproxyProviderService;
    private final ScreensModel screensModel;
    private final RuleService ruleService;

    @Autowired
    public ZaproxyInformationCollector(ScreensModel screensModel, ZaproxyProviderService zaproxyProviderService,
            RuleService ruleService) {
        this.screensModel = screensModel;
        this.zaproxyProviderService = zaproxyProviderService;
        this.ruleService = ruleService;
    }

    // this actually works if one would not want the same polling interval throughout the day:
    // @Scheduled(cron = "0 */5 7-12 * * MON-FRI")
    // @Scheduled(cron = "0 */15 13-23 * * MON-FRI")
    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.zaproxyInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    //@CacheEvict(cacheNames = {CACHE_NAME_TEAMCITY_BUILDS, CACHE_NAME_TEAMCITY_BUILD_STATISTICS}, allEntries = true)
    public void collectZaproxyInformationForScreens() {
        long start = System.currentTimeMillis();
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateZaproxyInformation);
        long stop = System.currentTimeMillis();
        long duration = stop - start;

        LOG.info("reading zaproxy status took: " + duration + "ms");
    }

    private void populateZaproxyInformation(Screen screen) {
        List<ZaproxyInformation> appInfoList = screen.getMatchingInformation(ZAPROXY);
        appInfoList.forEach(zaproxyProviderService::updateZaproxyInformation);

        ruleService.updateRuleEvaluation(screen, ZAPROXY);

        screen.setRefreshDate(ZAPROXY, new Date());
    }

}
