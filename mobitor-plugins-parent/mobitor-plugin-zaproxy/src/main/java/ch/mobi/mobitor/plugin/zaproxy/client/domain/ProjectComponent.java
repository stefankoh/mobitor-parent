package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class ProjectComponent {

    @JsonProperty
    private String id;
    @JsonProperty
    private String key;
    @JsonProperty
    private String name;
    @JsonProperty
    private String description;

    @JsonProperty("measures")
    private List<SonarQubeMeasurement> measures;

    @JsonCreator
    public ProjectComponent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SonarQubeMeasurement> getMeasures() {
        return measures;
    }

    public void setMeasures(List<SonarQubeMeasurement> measures) {
        this.measures = measures;
    }

    /* convenience methods */

    private BigDecimal getMeasurementValue(String metric) {
        for (SonarQubeMeasurement measure : measures) {
            if (measure.getMetric().equals(metric)) {
                return measure.getValue();
            }
        }
        return BigDecimal.ZERO;
    }

    public int getBlockers() {
        return getMeasurementValue("blocker_violations").intValue();
    }

    public int getCriticals() {
        return getMeasurementValue("critical_violations").intValue();
    }

    public int getMajors() {
        return getMeasurementValue("major_violations").intValue();
    }

    public int getMinors() {
        return getMeasurementValue("minor_violations").intValue();
    }

    public int getInfos() {
        return getMeasurementValue("info_violations").intValue();
    }

}
