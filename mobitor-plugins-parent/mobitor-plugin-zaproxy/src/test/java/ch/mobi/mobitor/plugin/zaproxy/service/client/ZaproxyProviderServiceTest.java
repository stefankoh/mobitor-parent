package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;

public class ZaproxyProviderServiceTest {

	public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

	private ZaproxySonarQubeClient client;

	@BeforeAll
	static void setup() {
		wiremock.start();
	}

	@BeforeEach
	void setupEach() {
		String baseUrl = "http://localhost:" + wiremock.port();
		ZaproxySonarQubeConfiguration zaproxyConfiguration = new ZaproxySonarQubeConfiguration();
		zaproxyConfiguration.setBaseUrl(baseUrl);

		client = new ZaproxySonarQubeClient(zaproxyConfiguration);
	}

	@AfterEach
	void after() {
		wiremock.resetAll();
	}

	@AfterAll
	static void clean() {
		wiremock.shutdown();
	}

	@Test
	public void updateProjectInformation() {
		// arrange
		final String projectKey = "ch.mobi.ovn:ovn";

		ZaproxyInformation zapInfo = new ZaproxyInformation(projectKey);

		wiremock.stubFor(get(urlMatching("/api/measures/component?(.*)")).withQueryParam("component", equalTo(projectKey))
				.withHeader("Accept", equalTo("application/json"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBodyFile("sonar-report-response-app2.json")));
		wiremock.stubFor(get(urlMatching("/api/ce/component?(.*)"))
				.withQueryParam("component", WireMock.equalTo(projectKey))
				.withHeader("Accept", WireMock.equalTo("application/json"))
				.willReturn(WireMock.aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBodyFile("sonar-task-response.json")));

		ZaproxyProviderService service = new ZaproxyProviderService(client);

		// act
		service.updateZaproxyInformation(zapInfo);

		// assert
		assertThat(zapInfo).isNotNull();
		assertThat(zapInfo.getBlockerNumber()).isEqualTo(1);
		assertThat(zapInfo.getCriticalNumber()).isEqualTo(2);
		assertThat(zapInfo.getMajorNumber()).isEqualTo(4);
		assertThat(zapInfo.getMinorNumber()).isEqualTo(8);
		assertThat(zapInfo.isInformationUpdated()).isFalse();
	}

}
