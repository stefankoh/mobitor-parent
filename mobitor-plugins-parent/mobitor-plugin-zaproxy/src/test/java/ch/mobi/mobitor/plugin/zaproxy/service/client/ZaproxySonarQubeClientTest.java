package ch.mobi.mobitor.plugin.zaproxy.service.client;

import ch.mobi.mobitor.plugin.zaproxy.client.domain.ProjectComponent;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.TaskList;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

public class ZaproxySonarQubeClientTest {

	public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

	private ZaproxySonarQubeClient client;

	@BeforeAll
	static void setup() {
		wiremock.start();
	}

	@BeforeEach
	void setupEach() {
		String baseUrl = "http://localhost:" + wiremock.port();
		ZaproxySonarQubeConfiguration zaproxyConfiguration = new ZaproxySonarQubeConfiguration();
		zaproxyConfiguration.setBaseUrl(baseUrl);

		client = new ZaproxySonarQubeClient(zaproxyConfiguration);
	}

	@AfterEach
	void after() {
		wiremock.resetAll();
	}

	@AfterAll
	static void clean() {
		wiremock.shutdown();
	}

	@Test
	public void retrieveProjectInformation() {
		// arrange
		final String projectKey = "ch.mobi.key";

		wiremock.stubFor(get(urlMatching("/api/measures/component?(.*)"))
				.withQueryParam("component", equalTo(projectKey))
				.withHeader("Accept", equalTo("application/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json")
						.withBodyFile("sonar-report-response-app1.json")));

		// act
		ProjectComponent projectComponent = client.retrieveProjectInformation(projectKey);

		// assert
		assertThat(projectComponent).isNotNull();
		assertThat(projectComponent.getId()).isNotNull();
		assertThat(projectComponent.getId()).isNotEqualTo(0);
		assertThat(projectComponent.getKey()).isNotNull();
		assertThat(projectComponent.getName()).isNotNull();
		assertThat(projectComponent.getDescription()).isNotNull();
		assertThat(projectComponent.getBlockers()).isNotEqualTo(0);
		assertThat(projectComponent.getCriticals()).isNotEqualTo(0);
		assertThat(projectComponent.getMajors()).isNotEqualTo(0);
		assertThat(projectComponent.getMinors()).isNotEqualTo(0);
		assertThat(projectComponent.getInfos()).isNotEqualTo(0);
	}

	@Test
	public void testRetrieveTaskList() {
		// arrange
		final String projectKey = "ch.mobi.key";

		wiremock.stubFor(get(urlMatching("/api/ce/component?(.*)"))
				.withQueryParam("component", equalTo(projectKey))
				.withHeader("Accept", equalTo("application/json"))
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "application/json")
						.withBodyFile("sonar-task-response.json")));

		// act
		TaskList tl = client.retrieveTaskList(projectKey);

		// assert
		assertThat(tl).isNotNull();
		assertThat(tl.getQueue()).isNotNull();
		assertThat(tl.getQueue()).hasSize(1);
		assertThat(tl.getCurrent()).isNotNull();
	}

}
