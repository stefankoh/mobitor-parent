package ch.mobi.mobitor.plugin.edwh.domain;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

public class EdwhDeploymentsInformation implements ApplicationInformation, VersionInformation {

    public static final String EDWH_DEPLOYMENT = "edwh_deployment";

    private String serverName;
    private String applicationName;
    private String environment;
    private String version;
    private String state;

    @Override
    public String getType() {
        return EDWH_DEPLOYMENT;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public boolean isSuccessful() {
        return "successful".equals(state);
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
