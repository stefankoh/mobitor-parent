package ch.mobi.mobitor.plugin.edwh.service.client;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.edwh.service.client.dto.EdwhServerConfig;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.contract.wiremock.WireMockSpring;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EdwhDeploymentsServiceTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockSpring.options().dynamicPort());

    private final static String SERVICE_BASE_PATH = "/dwh/etl/rest";

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    private void configureReleasesUrl() {
        wiremock.stubFor(get(urlMatching(SERVICE_BASE_PATH + "/releases/.*"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("dwh-etl-releases-response.json")
                )
        );
    }


    @Test
    public void retrieveDeploymentInformation() {
        // arrange
        String baseUrl = "http://localhost:" + wiremock.port() + SERVICE_BASE_PATH;
        configureReleasesUrl();
        EdwhConfigurationService configService = mock(EdwhConfigurationService.class);
        EdwhServerConfig edwhServerConfig = new EdwhServerConfig();
        edwhServerConfig.setEnvironment("BB");
        edwhServerConfig.setUrl(baseUrl);
        when(configService.getEdwhServerConfig(anyString())).thenReturn(edwhServerConfig);
        EdwhDeploymentsService service = new EdwhDeploymentsService(configService);

        // act
        EdwhDeployment edwhDeployment = service.retrieveDeploymentInformation("EDWHC", "QUERYSURGE", "BB");

        // assert
        assertThat(edwhDeployment).isNotNull();
        assertThat(edwhDeployment.getRelease()).isEqualTo("4444.0");
    }

}
