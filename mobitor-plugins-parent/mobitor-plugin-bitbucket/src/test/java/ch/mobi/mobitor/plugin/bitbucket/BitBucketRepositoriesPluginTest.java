package ch.mobi.mobitor.plugin.bitbucket;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.bitbucket.config.BitBucketRepositoryConfig;
import ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation;
import ch.mobi.mobitor.plugin.test.PluginTest;

import static org.mockito.Mockito.mock;

public class BitBucketRepositoriesPluginTest extends PluginTest<BitBucketRepositoriesPlugin, BitBucketRepositoryConfig, BitBucketInformation> {

    @Override
    public String getConfigPropertyName() {
        return "bitBucketRepositories";
    }

    @Override
    protected BitBucketRepositoriesPlugin newClassUnderTestInstance() {
        BitBucketPluginConfiguration bitBucketRepositoriesPlugin = mock(BitBucketPluginConfiguration.class);
        return new BitBucketRepositoriesPlugin(bitBucketRepositoriesPlugin);
    }

}
