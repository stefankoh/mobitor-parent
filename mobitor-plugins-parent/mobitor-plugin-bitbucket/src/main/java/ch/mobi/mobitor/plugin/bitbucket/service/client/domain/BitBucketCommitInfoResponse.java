package ch.mobi.mobitor.plugin.bitbucket.service.client.domain;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BitBucketCommitInfoResponse {

    @JsonProperty private String id;
    @JsonProperty private String displayId;
    @JsonProperty private long committerTimestamp;
    @JsonProperty private Map<String, Object> properties = new HashMap<>();

    @JsonIgnore
    public List<String> getJiraKeys() {
        Object jiraKeysObj = properties.get("jira-key");
        if (!(jiraKeysObj instanceof List)) {
            return Collections.emptyList();
        } else {
            return (List<String>) jiraKeysObj;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public long getCommitterTimestamp() {
        return committerTimestamp;
    }

    public void setCommitterTimestamp(long committerTimestamp) {
        this.committerTimestamp = committerTimestamp;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }
}
