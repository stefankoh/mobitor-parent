package ch.mobi.mobitor.plugin.bitbucket.domain;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BitBucketInformation implements ApplicationInformation {

    public static final String BITBUCKET = "bitbucket";

    private String serverName;
    private String applicationName;
    private String environment;
    private String project;
    private String repository;
    private String commitTimestamp;
    private String commitMessage;
    private String author;
    private long deploymentDate;
    private String bitBucketProjectsUrl;

    public BitBucketInformation() {
    }

    @Override
    public String getType() {
        return BITBUCKET;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public Date getCommitDate() {
        try {
            Date commitDate = new Date(Long.parseLong(commitTimestamp));
            return commitDate;
        } catch (Exception ignored){
        }
        return null;
    }

    public long getDaysSinceCommit() {
        if (commitTimestamp != null) {
            long diff = System.currentTimeMillis() - Long.parseLong(commitTimestamp);
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } else {
            return -1;
        }
    }

    public long getDaysSinceDeployment() {
        if (commitTimestamp != null) {
            long diff = deploymentDate - Long.parseLong(commitTimestamp);
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } else {
            return -1;
        }
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getRepository() {
        return repository;
    }

    public void setCommitTimestamp(String commitTimestamp) {
        this.commitTimestamp = commitTimestamp;
    }

    public String getCommitTimestamp() {
        return commitTimestamp;
    }

    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    public String getCommitMessage() {
        return commitMessage;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setDeploymentDate(long deploymentDate) {
        this.deploymentDate = deploymentDate;
    }

    public long getDeploymentDate() {
        return deploymentDate;
    }

    public void setBitBucketProjectsUrl(String bitBucketProjectsUrl) {
        this.bitBucketProjectsUrl = bitBucketProjectsUrl;
    }

    public String getBitBucketProjectsUrl() {
        return bitBucketProjectsUrl;
    }
}
