package ch.mobi.mobitor.plugin.bitbucket;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class BitBucketRepositoriesLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        BitBucketInformation bitBucketInfo = new BitBucketInformation();

        LocalDateTime commitDate = LocalDateTime.now();
        LocalDateTime deployDate = commitDate.plusDays(3);
        ZoneId zoneId = ZoneId.systemDefault();

        long commitLong = commitDate.atZone(zoneId).toInstant().toEpochMilli();
        long deployLong = deployDate.atZone(zoneId).toInstant().toEpochMilli();

        bitBucketInfo.setCommitTimestamp(Long.toString(commitLong));
        bitBucketInfo.setDeploymentDate(deployLong);
        bitBucketInfo.setCommitMessage("Commit message");
        bitBucketInfo.setProject("PROJECTNAME");
        bitBucketInfo.setRepository("REPONAME");
        bitBucketInfo.setBitBucketProjectsUrl("https://bitbucket.org/");

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("BitBucket Information", bitBucketInfo);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        return emptyList();
    }
}
