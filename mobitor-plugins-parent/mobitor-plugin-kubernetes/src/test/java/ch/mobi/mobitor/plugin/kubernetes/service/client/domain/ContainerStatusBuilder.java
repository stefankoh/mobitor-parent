package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.function.Consumer;

import static ch.mobi.mobitor.plugin.kubernetes.service.client.domain.ContainerStateBuilder.buildTerminatedWithDefaults;

public class ContainerStatusBuilder {

    private final ContainerStatus containerStatus;

    private ContainerStatusBuilder() {
        this.containerStatus = new ContainerStatus();
    }

    public static ContainerStatus buildContainerStatusWithDefaults() {
        return buildContainerStatusWithDefaults(builder -> {});
    }

    public static ContainerStatus buildContainerStatusWithDefaults(Consumer<ContainerStatusBuilder> builderValues) {
        Consumer<ContainerStatusBuilder> builderDefaults = builder -> builder.name("envchecker")
                                                                             .ready(true)
                                                                             .restartCount(0)
                                                                             .image("envchecker:0.0.1")
                                                                             .state(buildTerminatedWithDefaults());
        return buildContainerStatus(builderDefaults.andThen(builderValues));
    }

    public static ContainerStatus buildContainerStatus(Consumer<ContainerStatusBuilder> builderValues) {
        ContainerStatusBuilder builder = new ContainerStatusBuilder();
        builderValues.accept(builder);
        return builder.build();
    }

    private ContainerStatus build() {
        return containerStatus;
    }

    public ContainerStatusBuilder name(String name) {
        containerStatus.setName(name);
        return this;
    }

    public ContainerStatusBuilder ready(boolean ready) {
        containerStatus.setReady(ready);
        return this;
    }

    public ContainerStatusBuilder restartCount(int restartCount) {
        containerStatus.setRestartCount(restartCount);
        return this;
    }

    public ContainerStatusBuilder image(String image) {
        containerStatus.setImage(image);
        return this;
    }

    public ContainerStatusBuilder state(ContainerState state) {
        containerStatus.setState(state);
        return this;
    }

}
