package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobList;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.KubernetesServerConfig;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
public class KubernetesClientTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private KubernetesClient kubernetesClient;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port();

        KubernetesServerConfig localKubeConfig = new KubernetesServerConfig();
        localKubeConfig.setNetwork(EnvironmentNetwork.DEVELOPMENT);
        localKubeConfig.setUrl(baseUrl);
        localKubeConfig.setEnvironments(Sets.newHashSet("T1", "T2"));

        given(this.kubernetesConfigurationService.getKubernetesServerConfigForNetwork(EnvironmentNetwork.DEVELOPMENT)).willReturn(localKubeConfig);
        given(this.kubernetesConfigurationService.getKubernetesServerConfigForEnvironment(ArgumentMatchers.any())).willReturn(localKubeConfig);

        KubernetesRestClient kubernetesRestClient = new KubernetesRestClient();
        kubernetesClient = new KubernetesClient(kubernetesConfigurationService, kubernetesRestClient);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @MockBean
    public KubernetesConfigurationService kubernetesConfigurationService;


    @Test
    public void testJobIsRunning() {
        // arrange
        wiremock.stubFor(get(urlEqualTo("/apis/batch/v1/namespaces/pdn-b/jobs/?labelSelector=app=pdn-produktversion-job"))
                                   .withHeader("Accept", equalTo("application/json"))
                                   .willReturn(aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("k8s-job-running.json")
                )
        );

        // act
        JobList jobList = kubernetesClient.retrieveJobInformation("B", "pdn-b", "app=pdn-produktversion-job");

        // assert
        assertThat(jobList).isNotNull();
        assertThat(jobList.getItems()).isNotEmpty();
        assertThat(jobList.getItems().get(0).getStatus().getActive()).isEqualTo(1);
    }

    @Test
    public void testJobIsSucceeded() {
        // arrange
        wiremock.stubFor(get(urlEqualTo("/apis/batch/v1/namespaces/pdn-b/jobs/?labelSelector=app=pdn-produktversion-job"))
                                   .withHeader("Accept", equalTo("application/json"))
                                   .willReturn(aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("k8s-job-succesful.json")
                )
        );

        // act
        JobList jobList = kubernetesClient.retrieveJobInformation("B", "pdn-b", "app=pdn-produktversion-job");

        // assert
        assertThat(jobList).isNotNull();
        assertThat(jobList.getItems()).isNotEmpty();
        assertThat(jobList.getItems().get(0).getStatus().getSucceeded()).isEqualTo(1);
    }

}
