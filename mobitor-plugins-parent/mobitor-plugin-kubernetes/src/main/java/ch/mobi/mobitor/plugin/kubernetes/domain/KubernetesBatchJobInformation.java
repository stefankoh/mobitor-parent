package ch.mobi.mobitor.plugin.kubernetes.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import org.jetbrains.annotations.TestOnly;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class KubernetesBatchJobInformation implements ApplicationInformation {

    public static final String KUBERNETES_BATCH_JOB = "kubernetes_batch_job";

    private final String environment;
    private final String labelSelector;
    private String namespace;

    private LocalDateTime start;
    private LocalDateTime completion;
    private Duration duration;

    private boolean enabled = true;

    private JobStatusPhase status = null;

    public KubernetesBatchJobInformation(String environment, String labelSelector, String namespace) {
        this.labelSelector = labelSelector;
        this.environment = environment;
        this.namespace = namespace;
    }

    @Override
    public String getType() {
        return KUBERNETES_BATCH_JOB;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public String lastStart() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        return (start == null) ? null : start.format(formatter);
    }

    public String lastDuration() {
        if (duration == null) return null;
        long seconds = duration.getSeconds();
        String format = String.format("%dh %02dmin %02ds",
                seconds / 3600,
                (seconds % 3600) / 60,
                seconds % 60);
        return format;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getLabelSelector() {
        return labelSelector;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public void setCompletion(LocalDateTime completion) {
        this.completion = completion;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void calculateDuration() {
        if (start != null && completion != null) {
            duration = Duration.between(start, completion);
        }
    }

    public JobStatusPhase getStatus() {
        return status;
    }

    public void updateStatus(int active, int succeeded, List<JobCondition> conditions) {
        if (active == 1) {
            status = JobStatusPhase.RUNNING;
        }
        if (succeeded == 1) {
            status = JobStatusPhase.SUCCEEDED;
        }
    }

    @TestOnly
    public void setStatus(JobStatusPhase status) {
        this.status = status;
    }
}
