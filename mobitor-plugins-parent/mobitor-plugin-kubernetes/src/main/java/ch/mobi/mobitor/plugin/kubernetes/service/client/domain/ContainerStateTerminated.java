package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

/**
 * ContainerStateTerminated is a terminated state of a container.
 *
 * @see <a href="https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#containerstateterminated-v1-core">ContainerStateTerminated v1 core</a>
 */
public class ContainerStateTerminated {

    @JsonProperty
    private LocalDateTime startedAt;

    @JsonProperty
    private LocalDateTime finishedAt;

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public LocalDateTime getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(LocalDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }
}
