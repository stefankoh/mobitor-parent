= Mobitor Nexus IQ Plugin

link:../../index.html[Back Mobitor Overview]

== Features

Display a tile showing the total number of critical license or security violations of the project.

=== Screen information

If you hover the mouse over the Nexus IQ tile, it will indicate the following values :

- number of critical security violations
- number of critical license violations

Clicking on the tile forwards to the corresponding report in Nexus IQ. If no violation has been found, then the link sends to the application page.

== Configuration

[source, json]
----
{
    "publicId": "nexusiq.public.id", // <1>
    "serverName": "mobitor.server.name", // <2>
    "applicationName": "mobitor.application.name", // <3>
    "environment": "build", // <4>
    "stage": "build", // <5>
}
----
<1> MANDATORY - Public ID of your application inside Nexus IQ (see image below)
<2> MANDATORY - Name of the server defined inside the file application-servers.json. Often related to Liima names.
<3> MANDATORY - Name of the application defined inside the file application-servers.json. Often related to Liima names.
<4> MANDATORY - Name of the environment. Often it is 'build'. For components in production, must be 'P'
<5> OPTIONAL - Stage (developmente phase) of the Nexus IQ analysis during the development process: 
- "build": Nexus IQ analysis during development
- "release": Nexus IQ analysis at deployment

image::nexusiq-public-id.png[]


The policies that are queried are configured in `classpath:mobitor/plugin/nexusiq/config/nexus-iq-configuration.json`

[source, json]
----
{
  "policyIds": [
    {
      "id": "32characterpolicyId",
      "label": "Some Label"
    }
  ]
}
----


== Result

imagE:nexus-iq-report.png[]

The flag is green if none of the configured policies are triggering a report (security or license related)
