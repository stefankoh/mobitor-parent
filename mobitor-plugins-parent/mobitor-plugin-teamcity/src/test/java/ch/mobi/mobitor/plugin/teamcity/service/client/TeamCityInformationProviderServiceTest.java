package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;


public class TeamCityInformationProviderServiceTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private TeamCityInformationProviderService teamCityService;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port() + "/TeamCity";

        TeamCityConfiguration configuration = new TeamCityConfiguration();
        configuration.setBaseUrl(baseUrl);
        configuration.setUsername("junit");
        configuration.setPassword("junit");

        TeamCityClient client = new TeamCityClient(configuration);
        teamCityService = new TeamCityInformationProviderService(client);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @Test
    public void updateSuccessfulBuildInformation() {
        // arrange
        TeamCityBuildInformation build = new TeamCityBuildInformation("tcConfigId", "junit");

        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("([buildType:][a-zA-Z0-9_:,]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-ags-success.json")
                )
        );

        // act
        teamCityService.updateBuildInformation(build);

        // assert
        assertThat(build.getConfigId()).isEqualTo("tcConfigId");
        assertThat(build.isSuccessful()).isTrue();
        assertThat(build.isRunning()).isFalse();
    }

    @Test
    public void updateFailureBuildInformation() {
        // arrange
        TeamCityBuildInformation build = new TeamCityBuildInformation("tcConfigId", "junit");

        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("([buildType:][a-zA-Z0-9_:,]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-ags-failure.json")
                )
        );

        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds/id:(.*)/statistics"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-ags-failure-stats.json")
                )
        );

        // act
        teamCityService.updateBuildInformation(build);

        // assert
        assertThat(build.getConfigId()).isEqualTo("tcConfigId");
        assertThat(build.isSuccessful()).isFalse();
        assertThat(build.isRunning()).isFalse();

        assertThat(build.getTestsFailed()).isNotNull();
    }

    @Test
    public void updateProjectInformation() {
        // arrange
        TeamCityProjectInformation teamCityProjectInformation = new TeamCityProjectInformation("Zug_V2_E2e_V_Chrome", "junit");

        // create buildSuccess response
        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Zug_V2_E2e_v_Chrome_einfacher_fall[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-success.json")
                )
        );

        //create failureResponse
        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Zug_V2_E2e_v_Chrome_Szenario01[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-failure.json")
                )
        );


        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/projects/Zug_V2_E2e_V_Chrome/buildTypes"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-projects.json")
                )
        );

        // act
        teamCityService.updateProjectInformation(teamCityProjectInformation);

        // assert
        assertThat(teamCityProjectInformation.getStatus()).isEqualTo("FAILURE");
        assertThat(teamCityProjectInformation.getJobsPassed()).isEqualTo(1);
        assertThat(teamCityProjectInformation.getJobsTotal()).isEqualTo(2);
    }

    @Test
    public void emptyProjectBuilds() {
        // arrange
        TeamCityProjectInformation teamCityProjectInformation = new TeamCityProjectInformation("Zug_V2_E2e_V_Chrome", "junit");

        // create emptyResponse
        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Zug_V2_E2e_v_Chrome_Szenario01[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-empty.json")
                )
        );

        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/projects/Zug_V2_E2e_V_Chrome/buildTypes"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project.json")
                )
        );

        // act
        teamCityService.updateProjectInformation(teamCityProjectInformation);

        // assert
        assertThat(teamCityProjectInformation.getStatus()).isEqualTo("SUCCESS");
        assertThat(teamCityProjectInformation.getJobsPassed()).isEqualTo(1);
        assertThat(teamCityProjectInformation.getJobsFailed()).isEqualTo(0);
        assertThat(teamCityProjectInformation.getJobsTotal()).isEqualTo(1);
    }

    @Test
    public void updateCoverageInformation() {
        // arrange
        TeamCityCoverageInformation teamCityCoverageInformation = new TeamCityCoverageInformation("Jap_JapEnvchecker_CommitRc", "junit");

        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Jap_JapEnvchecker_CommitRc[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-envchecker-success.json")
                )
        );

        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds/id:(.*)/statistics"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-envchecker-success-stats.json")
                )
        );

        // act
        teamCityService.updateCoverageInformation(teamCityCoverageInformation);

        // assert
        assertThat(teamCityCoverageInformation.getStatus()).isEqualTo("SUCCESS");
        assertThat(teamCityCoverageInformation.getCoverage()).isEqualTo(new BigDecimal("98.8"));
    }

}
