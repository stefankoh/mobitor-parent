package ch.mobi.mobitor.plugin.teamcity.rule;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation.TEAM_CITY_BUILD;
import static org.assertj.core.api.Assertions.assertThat;

public class TeamCityBuildsPassRuleTest extends PipelineRuleTest<TeamCityBuildsPassRule> {

    @Override
    protected TeamCityBuildsPassRule createNewRule() {
        return new TeamCityBuildsPassRule();
    }

    @Test
    public void evaluateRuleAllGood() {
        // arrange
        Pipeline pipeline = createPipeline();
        TeamCityBuildInformation buildInformation = new TeamCityBuildInformation("junit-configId", "junit-rc");
        buildInformation.setStatus("SUCCESS");
        pipeline.addInformation(ENV, APP_NAME, buildInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation).isNotNull();
        assertThat(ruleEvaluation.hasErrors()).isFalse();
    }

    @Test
    public void evaluateRuleWithViolations() {
        // arrange
        Pipeline pipeline = createPipeline();
        TeamCityBuildInformation buildInformation = new TeamCityBuildInformation("junit-configId", "junit-rc");
        pipeline.addInformation(ENV, APP_NAME, buildInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation).isNotNull();
        assertThat(ruleEvaluation.hasErrors()).isTrue();
    }

    @Test
    public void validatesType() {
        assertThat(validatesType(null)).isFalse();
        assertThat(validatesType(TEAM_CITY_BUILD)).isTrue();
    }

}
