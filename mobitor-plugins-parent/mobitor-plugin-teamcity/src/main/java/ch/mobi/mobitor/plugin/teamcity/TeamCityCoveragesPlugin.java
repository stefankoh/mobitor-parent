package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityCoverageConfig;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.teamCityCoverages.enabled", havingValue = "true")
public class TeamCityCoveragesPlugin implements MobitorPlugin<TeamCityCoverageConfig> {

    @Override
    public String getConfigPropertyName() {
        return "teamCityCoverages";
    }

    @Override
    public Class<TeamCityCoverageConfig> getConfigClass() {
        return TeamCityCoverageConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<TeamCityCoverageConfig> configs) {
        for (TeamCityCoverageConfig coverageConfig : configs) {
            String serverName = coverageConfig.getServerName();
            String applicationName = coverageConfig.getApplicationName();

            String environment = coverageConfig.getEnvironment();

            TeamCityCoverageInformation tcInformation = createTeamCityCoverageInformation(coverageConfig);
            screen.addInformation(serverName, applicationName, environment, tcInformation);
        }

    }

    private TeamCityCoverageInformation createTeamCityCoverageInformation(TeamCityCoverageConfig coverageConfig) {
        String configId = coverageConfig.getConfigId();
        String label = coverageConfig.getLabel();
        return new TeamCityCoverageInformation(configId, label);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new TeamCityCoveragesLegendGenerator().getLegendList();
    }
}
