package ch.mobi.mobitor.plugin.teamcity.service.client.domain.project;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * represents the {@code <buildTypes></buildTypes>} section of a TeamCity response that queries {@code /app/rest/projects/{projectId}/buildTypes}
 *
 * ex: {@code https://TeamCity/httpAuth/app/rest/projects/Zug_B2e_B2eV2_McsE2e_V_Chrome/buildTypes/id:Zug_B2e_B2eV2_McsE2e_v_Chrome_Szenario01/builds/}
 */
public class ProjectBuildTypesResponse implements Serializable {

    @JsonProperty
    private long count;

    @JsonProperty("buildType")
    private List<ProjectBuildTypeResponse> projectBuildTypeResponses;

    @JsonCreator
    public ProjectBuildTypesResponse(){}

    public List<ProjectBuildTypeResponse> getProjectBuildTypeResponses() {
        return projectBuildTypeResponses;
    }

    public void setProjectBuildTypeResponses(List<ProjectBuildTypeResponse> projectBuildTypeResponses) {
        this.projectBuildTypeResponses = projectBuildTypeResponses;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
