= Mobitor DWH Plugin

link:../../index.html[Back Mobitor Overview]

== Features

This plugin retrieves deployment information of installed ETL packages from a service.

The service the plugin queries is configured in `classpath:mobitor/plugin/dwh/config/dwh-etl-servers.json`.


== Configuration

.screen.json
[source,json]
----
"dwhDeployments": [
  {
    "serverName": "DMSCHAD",
    "applications": [
      {"DATASTAGE": "Data Stage"},
      {"DATABASE": "SQL"},
      {"QUERYSURGE": "Query Surge"}
    ]
  }
----

.Parameters
|===
| Name | Description | Required | Default

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applications
| A list of objects associating application names with deployment service plugin names.
    Defines and refers to the sub-row of the server name in the same screen.
    Used to reference the position if this information block in the screen
| yes
| empty
|===

The plugin will use the `environments` from the same screen to query the service.


== Result

The plugin fills information blocks that provide version information on a screen:

image::dwh-information-block.png[]

Clicking on the cube icon leads to a separate page listing all deployments.

image::dwh-deployments-block.png[]
