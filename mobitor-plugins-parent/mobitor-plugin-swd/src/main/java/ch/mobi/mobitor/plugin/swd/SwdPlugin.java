package ch.mobi.mobitor.plugin.swd;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.swd.config.SwdDeploymentConfig;
import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.swdDeployments.enabled", havingValue = "true")
public class SwdPlugin implements MobitorPlugin<SwdDeploymentConfig> {

    private final EnvironmentsConfigurationService environmentsConfigurationService;

    @Autowired
    public SwdPlugin(EnvironmentsConfigurationService environmentsConfigurationService) {
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    @Override
    public String getConfigPropertyName() {
        return "swdDeployments";
    }

    @Override
    public Class<SwdDeploymentConfig> getConfigClass() {
        return SwdDeploymentConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig,
            List<SwdDeploymentConfig> configs) {
        for (SwdDeploymentConfig swdConfig : configs) {
            String serverName = swdConfig.getServerName();

            for (String environment : screenConfig.getEnvironments()) {
                EnvironmentConfigProperties envConfig = environmentsConfigurationService.getEnvironmentConfig(environment);
                if (!envConfig.isBuildEnvironment()) {
                    String applicationName = swdConfig.getApplicationName();

                    SwdDeploymentInformation information = new SwdDeploymentInformation(applicationName, environment);
                    screen.addInformation(serverName, applicationName, environment, information);
                }
            }
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new SwdLegendGenerator().getLegendList();
    }
}
