package ch.mobi.mobitor.plugin.swd.service.client.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2019 - 2020 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

/*

{"ID":280,"environment":"B","StartTime":"2020-01-29T14:07:05.35","EndTime":"2020-01-29T14:08:01.297","State":"Success","GitRevision":"392ff3373fda4ff77feba75c16abb67680466ee3","NrOfServer":9}

 */
public class ElanPatchStatus {

    @JsonProperty("ID")
    private String id;

    @JsonProperty("State")
    private String state;

    @JsonProperty("StartTime")
    private LocalDateTime startTime;

    @JsonProperty("GitRevision")
    private String gitRevision;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public String getGitRevision() {
        return gitRevision;
    }

    public void setGitRevision(String gitRevision) {
        this.gitRevision = gitRevision;
    }
}
