package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static java.time.LocalDateTime.of;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.swd.service.client.domain.ElanPatchStatus;

public class SwdDeploymentInformationProviderServiceTest {

    private SwdClient client;
    private SwdDeploymentInformationProviderService service;

    private SwdDeploymentInformation swdDeploymentInfo;

    @BeforeEach
    public void setUp() {
        client = mock(SwdClient.class);
        service = new SwdDeploymentInformationProviderService(client);

        swdDeploymentInfo = new SwdDeploymentInformation("PatchlevelTest", "P");
    }

    @Test
    public void updateSwdDeploymentInformationShouldNotUpdateInformationIfClientDoesNotReturnAResponse() {
        //given
        ElanPatchStatus eps = new ElanPatchStatus();
        when(client.retrieveSwdDeploymentInformation(any())).thenReturn(eps);

        //when
        service.updateSwdDeploymentInformation(asList(swdDeploymentInfo));

        //then
        assertThat(swdDeploymentInfo.getRevision()).isNull();
    }

    @Test
    public void updateSwdDeploymentInformationShouldUpdateInformationIfResponseIsReceived() {
        //given
        LocalDateTime patchDate = of(2011, 12, 13, 14, 15, 16);

        ElanPatchStatus eps = new ElanPatchStatus();
        eps.setId("Id-3");
        eps.setGitRevision("abcdefghijkl");
        eps.setState("Success");
        eps.setStartTime(patchDate);
        when(client.retrieveSwdDeploymentInformation(any())).thenReturn(eps);

        //when
        service.updateSwdDeploymentInformation(asList(swdDeploymentInfo));

        //then
        assertThat(swdDeploymentInfo.getRevision()).isEqualTo("abcdefghijkl");
        assertThat(swdDeploymentInfo.getId()).isEqualTo("Id-3");
    }

}
