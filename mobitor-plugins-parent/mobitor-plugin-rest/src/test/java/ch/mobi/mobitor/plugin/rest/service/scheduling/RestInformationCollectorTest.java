package ch.mobi.mobitor.plugin.rest.service.scheduling;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.swagger.PathHttpMethods;
import ch.mobi.mobitor.plugin.rest.domain.swagger.SwaggerResponse;
import ch.mobi.mobitor.plugin.rest.service.client.SwaggerClient;
import ch.mobi.mobitor.plugin.rest.service.restservice.AdditionalEndpointInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.interpreters.DefaultRestServiceHttpRequestConfiguration;
import ch.mobi.mobitor.plugin.rest.service.restservice.interpreters.Jes2HealthStatusInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.interpreters.JesStatusCodeInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.interpreters.RegexAndStatusCodeInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.interpreters.RestServiceHttpRequestExecutorAnonymous;
import ch.mobi.mobitor.plugin.rest.service.restservice.interpreters.RestServiceHttpRequestExecutorAuthenticating;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.apache.http.client.HttpResponseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RestInformationCollectorTest {

    private RestInformationCollector sut;
    private SwaggerClient swaggerClient;
    private RestCallMetricService restCallMetricService;

    @BeforeEach
    public void setUp() {
        ScreensModel screensModel = mock(ScreensModel.class);
        RuleService ruleService = mock(RuleService.class);
        restCallMetricService = mock(RestCallMetricService.class);
        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);
        swaggerClient = mock(SwaggerClient.class);
        List<SwaggerEndpointInterpreter> swaggerEndpointInterpreters = emptyList();
        List<AdditionalEndpointInterpreter> additionalEndpointInterpreters = emptyList();
        sut = new RestInformationCollector(screensModel, ruleService, restCallMetricService, collectorMetricService, swaggerClient, swaggerEndpointInterpreters, additionalEndpointInterpreters);

        Screen screen = mock(Screen.class);
        Mockito.when(screensModel.getAvailableScreens()).thenReturn(singletonList(screen));
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("htttp://junit.swagger.local/swagger.json");
        Mockito.when(screen.getMatchingInformation(ArgumentMatchers.eq(REST))).thenReturn(singletonList(restInfo));
    }

    @Test
    public void addSwaggerErrorResponseIfClientFailsWithHttpResponseException() throws IOException, URISyntaxException {
        //arrange
        doThrow(new HttpResponseException(418, "I'm a teapot")).when(swaggerClient).retrieveSwaggerJson(any());

        //act
        sut.collectRestCallsServiceResponses();

        //assert
        ArgumentCaptor<RestCallInformation> captor = ArgumentCaptor.forClass(RestCallInformation.class);
        verify(restCallMetricService).submitRestCallFailures(captor.capture());
        List<RestServiceResponse> restServiceResponses = captor.getValue().getRestServiceResponses();
        assertThat(restServiceResponses).hasSize(1)
                                        .extracting(RestServiceResponse::getStatusCode)
                                        .containsExactly(418);
    }

    @Test
    public void addGenericSwaggerErrorResponseIfClientFailsWithOtherIOException() throws IOException, URISyntaxException {
        //arrange
        doThrow(new IOException()).when(swaggerClient).retrieveSwaggerJson(any());

        //act
        sut.collectRestCallsServiceResponses();

        //assert
        ArgumentCaptor<RestCallInformation> captor = ArgumentCaptor.forClass(RestCallInformation.class);
        verify(restCallMetricService).submitRestCallFailures(captor.capture());
        List<RestServiceResponse> restServiceResponses = captor.getValue().getRestServiceResponses();
        assertThat(restServiceResponses).hasSize(1)
                                        .extracting(RestServiceResponse::getStatusCode)
                                        .containsExactly(444);
    }

    @Test
    public void addGenericSwaggerErrorResponseIfClientFailsWithOtherURISyntaxException() throws IOException, URISyntaxException {
        //arrange
        doThrow(new URISyntaxException("", "")).when(swaggerClient).retrieveSwaggerJson(any());

        //act
        sut.collectRestCallsServiceResponses();

        //assert
        ArgumentCaptor<RestCallInformation> captor = ArgumentCaptor.forClass(RestCallInformation.class);
        verify(restCallMetricService).submitRestCallFailures(captor.capture());
        List<RestServiceResponse> restServiceResponses = captor.getValue().getRestServiceResponses();
        assertThat(restServiceResponses).hasSize(1)
                                        .extracting(RestServiceResponse::getStatusCode)
                                        .containsExactly(444);
    }

    @Test
    public void notAddSwaggerErrorResponseIfClientSuccessfullyGetsResponse() throws IOException, URISyntaxException {
        //arrange
        when(swaggerClient.retrieveSwaggerJson(any())).thenReturn(new SwaggerResponse());

        //act
        sut.collectRestCallsServiceResponses();

        //assert
        ArgumentCaptor<RestCallInformation> captor = ArgumentCaptor.forClass(RestCallInformation.class);
        verify(restCallMetricService).submitRestCallFailures(captor.capture());
        List<RestServiceResponse> restServiceResponses = captor.getValue().getRestServiceResponses();
        assertThat(restServiceResponses).isEmpty();
    }

    @Test
    public void collectRestCallsServiceResponses() throws IOException, URISyntaxException {
        // arrange
        RuleService ruleService = mock(RuleService.class);
        RestCallMetricService restCallMetricService = mock(RestCallMetricService.class);
        SwaggerClient swaggerClientMock = mock(SwaggerClient.class);
        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);

        ScreensModel screensModel = Mockito.mock(ScreensModel.class);
        Screen screen = Mockito.mock(Screen.class);

        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("htttp://junit.swagger.local/swagger.json");
        List<ApplicationInformation> restInfoList = Collections.singletonList(restInfo);
//        screen.addInformation("appServ1", "app1", "Y", restInform);

        List<Screen> screensList = Collections.singletonList(screen);
        Mockito.when(screensModel.getAvailableScreens()).thenReturn(screensList);
        Mockito.when(screen.getMatchingInformation(ArgumentMatchers.eq(REST))).thenReturn(restInfoList);

        SwaggerResponse swaggerResponse = new SwaggerResponse();
        Map<String, PathHttpMethods> paths = new HashMap<>();
        PathHttpMethods httpMethod = new PathHttpMethods();
        httpMethod.add("get", null);
        paths.put("/health/status", httpMethod);
        paths.put("/rest/resource/ping", httpMethod);
        swaggerResponse.setPaths(paths);
        when(swaggerClientMock.retrieveSwaggerJson(anyString())).thenReturn(swaggerResponse);

        List<SwaggerEndpointInterpreter> interpreters = new ArrayList<>();
        interpreters.add(new Jes2HealthStatusInterpreter(
                new RestServiceHttpRequestExecutorAuthenticating(
                        new DefaultRestServiceHttpRequestConfiguration(new RestPluginConfiguration()), "EXAMPLEUSERNAME"))
        );
        RestServiceHttpRequestExecutorAnonymous restServiceHttpRequestExecutor = new RestServiceHttpRequestExecutorAnonymous(new DefaultRestServiceHttpRequestConfiguration(new RestPluginConfiguration()));
        interpreters.add(new JesStatusCodeInterpreter(restServiceHttpRequestExecutor));

        List<AdditionalEndpointInterpreter> additionalEndpointInterpreters = new ArrayList<>();
        additionalEndpointInterpreters.add(new RegexAndStatusCodeInterpreter(restServiceHttpRequestExecutor));

        RestInformationCollector ric = new RestInformationCollector(screensModel, ruleService, restCallMetricService, collectorMetricService, swaggerClientMock, interpreters, additionalEndpointInterpreters);

        // act
        ric.collectRestCallsServiceResponses();

        // assert
        verify(collectorMetricService).submitCollectorDuration(eq("rest"), anyLong());
        verify(swaggerClientMock).retrieveSwaggerJson(anyString());
        verify(ruleService).updateRuleEvaluation(any(Screen.class), eq(REST));
    }
}
