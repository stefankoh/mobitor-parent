package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.plugin.rest.ScreenConfigRestServiceHelper.getParsedRestServiceConfigs;
import static org.assertj.core.api.Assertions.assertThat;

public class ScreenConfigRestServiceHelperTest {

    private Map<String, String> envToStageMap;
    private List<RestServiceConfig> restServiceConfigs;

    @BeforeEach
    public void setup() {
        envToStageMap = new HashMap<>();
        envToStageMap.put("Y", "ci");
        envToStageMap.put("B", "test");
        envToStageMap.put("T", "preprod");

        restServiceConfigs = new ArrayList<>();
    }

    @Test
    public void getParsedRestServiceConfigsUsingEnvPlaceholder() {
        // arrange
        RestServiceConfig restServiceConfig = new RestServiceConfig();
        restServiceConfig.setSwaggerUri("https://prefix-${env}.hostname.domain/context/path/swagger.json");
        restServiceConfigs.add(restServiceConfig);

        // act
        List<RestServiceConfig> resultingRestConfigs = getParsedRestServiceConfigs(envToStageMap, restServiceConfigs);

        // assert
        assertThat(resultingRestConfigs.size()).isEqualTo(envToStageMap.values().size());
        List<String> swaggerUris = resultingRestConfigs.stream().map(RestServiceConfig::getSwaggerUri).collect(Collectors.toList());
        assertThat(swaggerUris).contains("https://prefix-y.hostname.domain/context/path/swagger.json");
        assertThat(swaggerUris).contains("https://prefix-b.hostname.domain/context/path/swagger.json");
        assertThat(swaggerUris).contains("https://prefix-t.hostname.domain/context/path/swagger.json");
    }

    @Test
    public void getParsedRestServiceConfigsUsingStagePlaceholder() {
        // arrange
        RestServiceConfig restServiceConfig = new RestServiceConfig();
        restServiceConfig.setSwaggerUri("https://prefix-${stage}.hostname.domain/context/path/swagger.json");
        restServiceConfigs.add(restServiceConfig);

        // act
        List<RestServiceConfig> resultingRestConfigs = getParsedRestServiceConfigs(envToStageMap, restServiceConfigs);

        // assert
        assertThat(resultingRestConfigs.size()).isEqualTo(envToStageMap.values().size());
        List<String> swaggerUris = resultingRestConfigs.stream().map(RestServiceConfig::getSwaggerUri).collect(Collectors.toList());
        assertThat(swaggerUris).contains("https://prefix-ci.hostname.domain/context/path/swagger.json");
        assertThat(swaggerUris).contains("https://prefix-test.hostname.domain/context/path/swagger.json");
        assertThat(swaggerUris).contains("https://prefix-preprod.hostname.domain/context/path/swagger.json");
    }

}
