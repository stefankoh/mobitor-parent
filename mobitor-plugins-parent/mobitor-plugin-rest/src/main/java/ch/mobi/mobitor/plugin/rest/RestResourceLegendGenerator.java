package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.FlywayRestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class RestResourceLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        RestServiceResponse rsr = new RestServiceResponse("/some/rest/resource", 200, SUCCESS, emptyList(), 150);
        RestCallInformation restCallInformation = new RestCallInformation();
        restCallInformation.setRestServiceResponses(singletonList(rsr));

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Good REST resources", restCallInformation);

        RestServiceResponse slowRsr = new RestServiceResponse("/some/slow/resource", 200, SUCCESS, emptyList(), 150_000);
        RestCallInformation slowRestCallInformation = new RestCallInformation();
        slowRestCallInformation.setRestServiceResponses(singletonList(slowRsr));

        ApplicationInformationLegendWrapper wrapperSlow = new ApplicationInformationLegendWrapper("Too slow REST resources", slowRestCallInformation);

        FlywayRestServiceResponse rsrFly = new FlywayRestServiceResponse("/some/rest/resource", 200, SUCCESS, emptyList(), 150);
        rsrFly.setVersion("1.17");
        rsrFly.setState("SUCCESS");
        rsrFly.setInstalledOn(new Date());
        RestCallInformation restCallInformationFly = new RestCallInformation();
        restCallInformationFly.setRestServiceResponses(singletonList(rsrFly));

        ApplicationInformationLegendWrapper wrapperFlyway = new ApplicationInformationLegendWrapper("Good REST resources with Flyway Information", restCallInformationFly);
        return asList(wrapper, wrapperFlyway, wrapperSlow);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        RestServiceResponse rsr = new RestServiceResponse("/some/rest/resource", 504, FAILURE, emptyList(), 150);
        RestCallInformation restCallInformation = new RestCallInformation();
        restCallInformation.setRestServiceResponses(singletonList(rsr));

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Failed REST resources", restCallInformation);
        return singletonList(wrapper);
    }
}
