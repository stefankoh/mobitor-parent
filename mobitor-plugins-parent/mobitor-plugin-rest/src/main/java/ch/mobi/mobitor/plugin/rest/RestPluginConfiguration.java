package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@ConfigurationProperties(prefix = "mobitor.plugins.rest")
public class RestPluginConfiguration {

    private final static Logger LOG = LoggerFactory.getLogger(RestPluginConfiguration.class);

    private EnvironmentNetwork network = EnvironmentNetwork.LOCALHOST;
    private String version;

    @Autowired
    public RestPluginConfiguration() {
    }

    public EnvironmentNetwork getNetwork() {
        return network;
    }

    public void setNetwork(EnvironmentNetwork network) {
        this.network = network;
    }

    @PostConstruct
    public void logValues() {
        LOG.info("Network: " + network);
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
