package ch.mobi.mobitor.plugin.rest.domain.appinfo;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppInfoResponse {

    @JsonProperty private String appName ; // "ens"
    @JsonProperty private String artefactVersion; //  "1.0.0-100"
    @JsonProperty private String osName; // "Linux"
    @JsonProperty private String lang; // "java"
    @JsonProperty private String langSpecVersion; // "1.8"
    @JsonProperty private String langVersion; // "1.8.0_161"
    @JsonProperty private String runtimeEnvironment; // "jvm"
    @JsonProperty private String appServer; // "jboss"
    @JsonProperty private String appServerVersion; // "7.1.0-1"
    @JsonProperty private String stackName; // "JES7"
    @JsonProperty private String stackVersion; // "1.2.0"
    @JsonProperty private String tkNameId; // vvn-auskunft-service

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getArtefactVersion() {
        return artefactVersion;
    }

    public void setArtefactVersion(String artefactVersion) {
        this.artefactVersion = artefactVersion;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLangSpecVersion() {
        return langSpecVersion;
    }

    public void setLangSpecVersion(String langSpecVersion) {
        this.langSpecVersion = langSpecVersion;
    }

    public String getLangVersion() {
        return langVersion;
    }

    public void setLangVersion(String langVersion) {
        this.langVersion = langVersion;
    }

    public String getRuntimeEnvironment() {
        return runtimeEnvironment;
    }

    public void setRuntimeEnvironment(String runtimeEnvironment) {
        this.runtimeEnvironment = runtimeEnvironment;
    }

    public String getAppServer() {
        return appServer;
    }

    public void setAppServer(String appServer) {
        this.appServer = appServer;
    }

    public String getAppServerVersion() {
        return appServerVersion;
    }

    public void setAppServerVersion(String appServerVersion) {
        this.appServerVersion = appServerVersion;
    }

    public String getStackName() {
        return stackName;
    }

    public void setStackName(String stackName) {
        this.stackName = stackName;
    }

    public String getStackVersion() {
        return stackVersion;
    }

    public void setStackVersion(String stackVersion) {
        this.stackVersion = stackVersion;
    }

    public String getTkNameId() {
        return tkNameId;
    }

    public void setTkNameId(String tkNameId) {
        this.tkNameId = tkNameId;
    }
}
