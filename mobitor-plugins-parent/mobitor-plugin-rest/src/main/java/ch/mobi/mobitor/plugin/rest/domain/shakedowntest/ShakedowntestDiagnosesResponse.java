package ch.mobi.mobitor.plugin.rest.domain.shakedowntest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShakedowntestDiagnosesResponse {

    @JsonProperty("callee") private String callee;
    @JsonProperty("state") private String state;
    @JsonProperty("technicalMessage") private String technicalMessage;
    @JsonProperty("childShakedownResultDTO") private ShakedowntestResponse childShakedowntestResponse;

    public ShakedowntestDiagnosesResponse() {
    }

    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTechnicalMessage() {
        return technicalMessage;
    }

    public void setTechnicalMessage(String technicalMessage) {
        this.technicalMessage = technicalMessage;
    }

    public ShakedowntestResponse getChildShakedowntestResponse() {
        return childShakedowntestResponse;
    }

    public void setChildShakedowntestResponse(ShakedowntestResponse childShakedowntestResponse) {
        this.childShakedowntestResponse = childShakedowntestResponse;
    }
}
