package ch.mobi.mobitor.plugin.rest.domain.shakedowntest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ShakedowntestResponse {

    @JsonProperty("diagnoses")
    private List<ShakedowntestDiagnosesResponse> diagnoses;

    public ShakedowntestResponse() {
    }

    public List<ShakedowntestDiagnosesResponse> getDiagnoses() {
        return diagnoses;
    }

    public void setDiagnoses(List<ShakedowntestDiagnosesResponse> diagnoses) {
        this.diagnoses = diagnoses;
    }

    @JsonIgnore
    public boolean hasAnyErrors() {
        return hasAnyErrors(this);
    }

    private boolean hasAnyErrors(ShakedowntestResponse shakedowntestResponse) {
        if (shakedowntestResponse == null || CollectionUtils.isEmpty(shakedowntestResponse.getDiagnoses())) {
            return false;
        }

        boolean hasErrors = false;
        for (ShakedowntestDiagnosesResponse diagnose : shakedowntestResponse.getDiagnoses()) {
            hasErrors = hasErrors || !"OK".equals(diagnose.getState());
            ShakedowntestResponse childResponse = diagnose.getChildShakedowntestResponse();
            hasErrors = hasErrors || hasAnyErrors(childResponse);
        }

        return hasErrors;
    }

    public List<String> getTechnicalMessages() {
        List<String> technicalMessages = new ArrayList<>();
        collectTechnicalMessages(this, technicalMessages);

        return technicalMessages;
    }

    private void collectTechnicalMessages(ShakedowntestResponse shakedowntestResponse, List<String> technicalMessages) {
        if (shakedowntestResponse == null || CollectionUtils.isEmpty(shakedowntestResponse.getDiagnoses())) {
            return;
        }

        for (ShakedowntestDiagnosesResponse diagnose : shakedowntestResponse.getDiagnoses()) {
            String msg = diagnose.getTechnicalMessage();
            if (StringUtils.hasText(msg)) {
                String technicalMessage = diagnose.getCallee() + ":" + diagnose.getState() + ":" + msg;
                technicalMessages.add(technicalMessage);
            }

            ShakedowntestResponse childResponse = diagnose.getChildShakedowntestResponse();
            collectTechnicalMessages(childResponse, technicalMessages);
        }
    }

}
