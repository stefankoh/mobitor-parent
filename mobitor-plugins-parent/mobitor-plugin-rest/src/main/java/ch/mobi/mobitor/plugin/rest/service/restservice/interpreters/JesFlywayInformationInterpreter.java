package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.FlywayRestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.interpreters.flyway.FlywayResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;

@Component
public class JesFlywayInformationInterpreter implements SwaggerEndpointInterpreter {

    private final static Logger LOG = LoggerFactory.getLogger(JesFlywayInformationInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public JesFlywayInformationInterpreter(@Qualifier("authenticating") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        Predicate<String> jes2 = path -> path.contains("/health/flyway/current");
        Predicate<String> jes7 = path -> path.contains("/jap/flyway/schema/version/current");

        return jes2.or(jes7);
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            FlywayResponse flywayResponse = createFlywayResponse(json);
            if (flywayResponse == null) {
                return FlywayRestServiceResponse.createErrorRestServiceResponse(uri, -70);

            } else {
                List<String> messages = new ArrayList<>();
                FlywayRestServiceResponse fwRestResp = new FlywayRestServiceResponse(uri, statusCode, SUCCESS, messages, -1);
                updateFlywayInformation(flywayResponse, fwRestResp);

                return fwRestResp;
            }

        } catch (Exception e) {
            LOG.error("Could not query: " + uri, e);
            return FlywayRestServiceResponse.createErrorRestServiceResponse(uri, -70);
        }
    }

    private FlywayResponse createFlywayResponse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try {
            FlywayResponse flywayResponse = mapper.readValue(json, FlywayResponse.class);
            return flywayResponse;

        } catch (Exception e) {
            return null;
        }
    }

    private void updateFlywayInformation(FlywayResponse flywayResponse, FlywayRestServiceResponse flywayRestServiceResponse) {
        flywayRestServiceResponse.setVersion(flywayResponse.getVersion());
        flywayRestServiceResponse.setState(flywayResponse.getState());
        flywayRestServiceResponse.setInstalledOn(flywayResponse.getInstalledOn());
        flywayRestServiceResponse.setScript(flywayResponse.getScript());
    }

}
