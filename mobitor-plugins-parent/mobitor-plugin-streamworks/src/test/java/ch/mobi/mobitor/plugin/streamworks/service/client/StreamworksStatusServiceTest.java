package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StreamworksStatusServiceTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private StreamworksStatusService service;

    private final static String SERVICE_BASE_PATH = "/sworkswww/data/verarbeitungen/W";

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port() + SERVICE_BASE_PATH;
        configureReleasesUrl();
        StreamworksConfigurationService configService = mock(StreamworksConfigurationService.class);
        StreamworksServerConfig serverConfig = new StreamworksServerConfig(baseUrl, "BB");
        when(configService.getStreamworksServerConfig(anyString())).thenReturn(serverConfig);

        service = new StreamworksStatusService(configService);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    private void configureReleasesUrl() {
        wiremock.stubFor(get(urlMatching("/.*")).willReturn(aResponse().withStatus(404)));

        wiremock.stubFor(get(urlMatching(SERVICE_BASE_PATH + "/EDWH/.*"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("streamworks-status-response.json")));
    }

    @Test
    public void retrieveStatusInformation() {
        // arrange
        StreamworksStatus expectedStatus = new StreamworksStatus("EDWH", LocalDate.of(2019, 5, 15), "Prepared");

        // act
        StreamworksStatus status = service.retrieveStreamworksInformation("EDWH", "BB");
        StreamworksStatus nonExistentStatus = service.retrieveStreamworksInformation("EDWHC", "BB");

        // act
        assertEquals(expectedStatus, status);
        assertNull(nonExistentStatus);
    }

}
