package ch.mobi.mobitor.plugin.streamworks.service.client.dto;

import ch.mobi.mobitor.plugin.streamworks.StreamworksConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.el.stream.Stream;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */
public class StreamworksJob {

    @JsonProperty("JobName")
    private String jobName;
    @JsonProperty("ExecutionNo")
    private Long executionNo;
    @JsonProperty("JobReturnCode")
    private String jobReturnCode;
    @JsonProperty("JobStatus")
    private String jobStatus;
    @JsonProperty("JobStartDateTime")
    private LocalDateTime jobStartDateTime;
    @JsonProperty("JobEndDateTime")
    private LocalDateTime jobEndDateTime;
    @JsonProperty("StreamName")
    private String streamName;


    // Constructor for Jackson deserializer
    public StreamworksJob() {
    }

    public StreamworksJob( String jobName,
                           Long executionNo,
                           String jobReturnCode,
                           String jobStatus,
                           String jobStartDateTime,
                           String jobEndDateTime,
                           String streamName) {
        this.jobName = jobName;
        this.executionNo = executionNo;
        this.jobReturnCode = jobReturnCode;
        this.jobStatus = jobStatus;
        this.jobStartDateTime = LocalDateTime.parse(jobStartDateTime, StreamworksConstant.INPUT_FORMATTER);
        this.jobEndDateTime = LocalDateTime.parse(jobEndDateTime, StreamworksConstant.INPUT_FORMATTER);
        this.streamName = streamName;
    }


    public String getJobName() {
        return jobName;
    }

    public Long getExecutionNo() {
        return executionNo;
    }

    public String getJobReturnCode() {
        return jobReturnCode;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public LocalDateTime getJobStartDateTime() {
        return jobStartDateTime;
    }

    public String getParsedStartTime(){
        return jobStartDateTime.format(StreamworksConstant.FORMATTER);
    }

    public LocalDateTime getJobEndDateTime() {
        return jobEndDateTime;
    }

    public String getParsedEndTime(){
        return jobEndDateTime.format(StreamworksConstant.FORMATTER);
    }

    public String getStreamName() {
        return streamName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setExecutionNo(Long executionNo) {
        this.executionNo = executionNo;
    }

    public void setJobReturnCode(String jobReturnCode) {
        this.jobReturnCode = jobReturnCode;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public void setJobStartDateTime(String jobStartDateTime) {
        this.jobStartDateTime = LocalDateTime.parse(jobStartDateTime, StreamworksConstant.INPUT_FORMATTER);
    }

    public void setJobEndDateTime(String jobEndDateTime) {
        this.jobEndDateTime = LocalDateTime.parse(jobEndDateTime, StreamworksConstant.INPUT_FORMATTER);
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        StreamworksJob that = (StreamworksJob) obj;
        return streamName.equals(that.streamName) &&
                jobName.equals(that.jobName) &&
                jobStatus.equals(that.jobStatus) &&
                executionNo.equals(that.executionNo) &&
                jobReturnCode.equals(that.jobReturnCode) &&
                jobStartDateTime.equals(that.jobStartDateTime) &&
                jobEndDateTime.equals(that.jobEndDateTime);


    }

    @Override
    public String toString() {
        return "StreamworksJob{" +
                "jobName='" + jobName + '\'' +
                ", executionNo=" + executionNo +
                ", jobReturnCode='" + jobReturnCode + '\'' +
                ", jobStatus='" + jobStatus + '\'' +
                ", jobStartDateTime=" + jobStartDateTime.format(StreamworksConstant.FORMATTER) +
                ", jobEndDateTime=" + jobEndDateTime.format(StreamworksConstant.FORMATTER) +
                ", streamName='" + streamName + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobName, executionNo, jobReturnCode, jobStatus, jobStartDateTime, jobEndDateTime, streamName);
    }
}
