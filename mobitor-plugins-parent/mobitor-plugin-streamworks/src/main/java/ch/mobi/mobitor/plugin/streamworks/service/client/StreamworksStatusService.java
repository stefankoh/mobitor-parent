package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.WebClients;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class StreamworksStatusService {
    private static final Logger LOG = LoggerFactory.getLogger(StreamworksStatusService.class);

    private final StreamworksConfigurationService configurationService;

    @Autowired
    public StreamworksStatusService(StreamworksConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Nullable
    public StreamworksStatus retrieveStreamworksInformation(String serverName, String environment) {
        StreamworksServerConfig config = configurationService.getStreamworksServerConfig(environment);
        if (config != null) {
            String url = config.getUrl() + "/" + serverName + "/" + LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
            try {
                return WebClients.withKeyStore().get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(StreamworksStatus.class)
                        .block();
            } catch (Exception ex) {
                LOG.error("Could not read streamworks state from {}", url, ex);
            }
        }
        return null;
    }

}
