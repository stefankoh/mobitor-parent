package ch.mobi.mobitor.plugin.streamworks.domain;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugin.streamworks.StreamworksLastRunPlugin;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksJob;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.time.LocalDateTime;
import java.util.List;

public class StreamworksLastRunStatusInformation implements ApplicationInformation {
    public static final String STREAMWORKS_LAST_RUN_STATUS = "streamworksLastRunStatus";

    private final String applicationName;
    private final String label;
    private final String environment;
    private String version;
    private StreamworksLastRunState state;
    private String verarbeitung;
    private List<StreamworksJob> jobList;

    public StreamworksLastRunStatusInformation(String applicationName, String label, String environment){
        this.applicationName = applicationName;
        this.label = label;
        this.environment = environment;
    }

    @Override
    public String getType() {
        return STREAMWORKS_LAST_RUN_STATUS;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }


    public String getApplicationName() {
        return applicationName;
    }

    public String getLabel() {
        return label;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setState(StreamworksLastRunState state) {
        this.state = state;
    }

    public StreamworksLastRunState getState() {
        return state;
    }

    public boolean isSuccessful(){
        if (state != null){
            return this.state.equals(StreamworksLastRunState.COMPLETED);
        }
        return false;

    }

    public boolean isRunning(){
        if (state != null){
            return this.state.equals(StreamworksLastRunState.RUNNING);
        }
        return false;

    }

    public static String getStreamworksLastRunStatus() {
        return STREAMWORKS_LAST_RUN_STATUS;
    }

    public String getVerarbeitung() {
        return verarbeitung;
    }

    public void setVerarbeitung(String verarbeitung) {
        this.verarbeitung = verarbeitung;
    }

    public List<StreamworksJob> getJobList() {
        return jobList;
    }

    public void setJobList(List<StreamworksJob> jobList) {
        this.jobList = jobList;
    }
}
