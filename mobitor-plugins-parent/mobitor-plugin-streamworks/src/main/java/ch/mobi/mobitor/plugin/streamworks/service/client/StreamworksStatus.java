package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.Objects;

public class StreamworksStatus {
    private final String verarbeitung;
    private final LocalDate planDate;
    private final String status;

    public StreamworksStatus(@JsonProperty("Verarbeitung") String verarbeitung,
                             @JsonProperty("PlanDate") LocalDate planDate,
                             @JsonProperty("Status") String status) {
        this.verarbeitung = verarbeitung;
        this.planDate = planDate;
        this.status = status;
    }

    public String getVerarbeitung() {
        return verarbeitung;
    }

    public LocalDate getPlanDate() {
        return planDate;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreamworksStatus that = (StreamworksStatus) o;
        return verarbeitung.equals(that.verarbeitung) &&
                planDate.equals(that.planDate) &&
                status.equals(that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(verarbeitung, planDate, status);
    }

    @Override
    public String toString() {
        return "StreamworksStatus{" +
                "verarbeitung='" + verarbeitung + '\'' +
                ", planDate=" + planDate +
                ", status='" + status + '\'' +
                '}';
    }
}
