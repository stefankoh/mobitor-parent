package ch.mobi.mobitor.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.model.EnvironmentTimestampModel;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.config.environment.EnvironmentConfig;
import ch.mobi.mobitor.plugin.rest.service.configservice.DefaultEnvironmentsConfigurationService;
import ch.mobi.mobitor.service.config.MobitorApplicationConfiguration;
import ch.mobi.mobitor.service.environment.EnvironmentTimestamps;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class EnvironmentTimestampCollectorTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private DefaultEnvironmentsConfigurationService envConfigService;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port() + "/db2stats/timestamp/current";

        envConfigService = new DefaultEnvironmentsConfigurationService(new DefaultResourceLoader(), new RestPluginConfiguration());
        List<EnvironmentConfig> envConfigs = new ArrayList<>();
        EnvironmentConfig envConf = new EnvironmentConfig();
        envConf.setTimestampUrl(baseUrl);
        envConf.setEnvironment("J");
        envConfigs.add(envConf);
        envConfigService.updateEnvironmentConfigs(envConfigs);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @Test
    public void collectDb2Timestamp() {
        // arrange
        wiremock.stubFor(get(urlPathEqualTo("/db2stats/timestamp/current"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("timestamp_current.json")
                )
        );

        RestTemplate restTemplate = new RestTemplateBuilder().build();

        EnvironmentTimestampModel envTimestampModelSpy = spy(new EnvironmentTimestampModel());
        MobitorApplicationConfiguration appConfig = new MobitorApplicationConfiguration(new MockEnvironment());
        appConfig.setNetwork(EnvironmentNetwork.PRODUCTION);

        EnvironmentTimestampCollector collector = new EnvironmentTimestampCollector(envTimestampModelSpy, restTemplate, envConfigService);

        // act
        collector.collectDb2Timestamp();

        // assert
        EnvironmentTimestamps timestamp = envTimestampModelSpy.getTimestamps("J");

        verify(envTimestampModelSpy).updateTimestamp(anyString(), any());
        assertThat(timestamp).isNotNull();
        assertThat(timestamp.getCurrentTime()).isNotNull();
        assertThat(timestamp.getShiftedTime()).isNotNull();
        assertThat(timestamp.getShiftedDays()).isEqualTo(0);
    }

}
