= Mobitor Rules

Rules are implementations of the `PipelineRule` interface. They mark a row as red if the rule they
are responsible for is not satisfied.

Examples include:

 * Code Coverage is below a limit
 * Build is failed
 * Deployed version contains an invalid format or is a SNAPSHOT version
 * The server must have an information block of type "NEXUSIQ" but none was found

Have a look at the included implementations.


A rule will get passed a "pipeline" instance which contains all information blocks for that
server (a row in the Mobitor).

The rule decides which information block types it validates.

If a rule is violated the server in the first column is marked red and a flash icon is shown
which contains a link to why there is a violation:

image::images/rule-violated.png[]

Rules can also provide hints and recommendations if there is no urgent need for changes:

image::images/rule-notification.png[]

If everything works to plan, there is no link but a trophy to celebrate your success!

image::images/rule-world-is-good.png[]
