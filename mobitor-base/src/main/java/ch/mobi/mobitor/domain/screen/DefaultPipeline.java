package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultPipeline implements Pipeline {

    private String appServerName;
    private Date lastRuleCompliantDate;
    private Date lastRuleUpdateDate;

    /**
     * stores the {@link DefaultServerContext} using the environment as key ("X" or "Y" etc.). The pipeline must contain one entry for
     * every environment displayed in a {@link Screen}
     */
    private Map<String, DefaultServerContext> serverContextMap = new HashMap<>();
    private ListMultimap<String, RuleEvaluation> ruleEvaluationMap = ArrayListMultimap.create();
    /**
     * currently the UI iterates over servers, then applications in the server, the applications are actually stored
     * per ServerContext, so this is maintained for convenience
     */
    private Set<String> applicationNames = new LinkedHashSet<>();

    public DefaultPipeline(String appServerName, Map<String, DefaultServerContext> serverContextMap) {
        this.appServerName = appServerName;
        this.serverContextMap.putAll(serverContextMap);
    }

    @Override
    public String getAppServerName() {
        return appServerName;
    }

    @Override
    public void addInformation(String environment, String applicationName, ApplicationInformation information) {
        DefaultServerContext serverContext = this.serverContextMap.get(environment);
        if (serverContext == null) {
            throw new IllegalStateException("Environment [" + environment + "] is not declared in the environments list");
        }
        serverContext.addInformation(applicationName, information);

        applicationNames.add(applicationName);
    }

    @Override
    public List<ApplicationInformation> getMatchingInformation(String type, String environment, String applicationName) {
        DefaultServerContext serverContext = this.serverContextMap.get(environment);
        List<ApplicationInformation> informationList = serverContext.getMatchingInformation(type, applicationName);
        return informationList;
    }

    public DefaultServerContext getServerContext(String environment) {
        return serverContextMap.get(environment);
    }

    @Override
    public Map<String, ServerContext> getServerContextMap() {
        return Collections.unmodifiableMap(this.serverContextMap);
    }

    @Override
    public void updateRuleEvaluation(String informationType, RuleEvaluation ruleEvaluation) {
        this.ruleEvaluationMap.put(informationType, ruleEvaluation);
        this.lastRuleUpdateDate = new Date();
    }

    @Override
    public void resetRuleEvaluation(String type) {
        this.ruleEvaluationMap.removeAll(type);
        this.lastRuleUpdateDate = new Date();
    }

    @Override
    public RuleEvaluationSeverity getRuleEvaluationSeverity() {
        if (!hasNoViolations()) {
            return RuleEvaluationSeverity.CONTAINS_ERRORS;
        }
        if (!hasNoWarnings()) {
            return RuleEvaluationSeverity.WARNINGS_ONLY;
        }
        return RuleEvaluationSeverity.TROPHY;
    }

    private boolean hasNoViolations() {
        for (RuleEvaluation ruleEvaluation : this.ruleEvaluationMap.values()) {
            if (ruleEvaluation.hasErrors()) {
                return false;
            }
        }
        return true;
    }

    private boolean hasNoWarnings() {
        for (RuleEvaluation ruleEvaluation : this.ruleEvaluationMap.values()) {
            if (ruleEvaluation.hasWarnings()) {
                return false;
            }
        }
        return true;
    }

    public List<String> getViolatedTypes() {
        return this.ruleEvaluationMap.asMap().entrySet().stream()
                                     .filter(map -> map.getValue().size() > 0)
                                     .map(Map.Entry::getKey)
                                     .collect(Collectors.toList());
    }

    public List<RuleEvaluation> getRuleEvaluations(String type) {
        return ruleEvaluationMap.get(type);
    }

    public Date getLastRuleCompliantDate() {
        return lastRuleCompliantDate;
    }

    @Override
    public void setLastRuleCompliantDate(Date date) {
        this.lastRuleCompliantDate = date;
    }

    public Date getLastRuleUpdateDate() {
        return lastRuleUpdateDate;
    }

    @Override
    public Set<String> getApplicationNames() {
        return applicationNames;
    }

}
