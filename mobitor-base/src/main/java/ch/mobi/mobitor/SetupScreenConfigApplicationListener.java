package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.factory.ExtendableScreenFactory;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.config.ExtendableScreenConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SetupScreenConfigApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(SetupScreenConfigApplicationListener.class);

    private final ScreensModel screensModel;
    private final ExtendableScreenConfigService extendableScreenConfigService;
    private final ExtendableScreenFactory extendableScreenFactory;

    @Autowired
    public SetupScreenConfigApplicationListener(ScreensModel screensModel,
                                                ExtendableScreenConfigService extendableScreenConfigService,
                                                ExtendableScreenFactory extendableScreenFactory) {
        this.screensModel = screensModel;
        this.extendableScreenConfigService = extendableScreenConfigService;
        this.extendableScreenFactory = extendableScreenFactory;
    }

    private void addEmptyScreen(ExtendableScreenConfig screenConfig) {
        Screen screen = extendableScreenFactory.initializeEmptyScreen(screenConfig);
        screensModel.addScreen(screen);
        LOG.debug("added extendable screen to model: " + screen.getConfigKey());
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        LOG.info("application started, initialize screen configurations...");

        List<ExtendableScreenConfig> extendableScreenConfigs = extendableScreenConfigService.getConfigs();
        extendableScreenConfigs.forEach(this::addEmptyScreen);

        LOG.info("Empty screens have been initialized.");
    }

}
