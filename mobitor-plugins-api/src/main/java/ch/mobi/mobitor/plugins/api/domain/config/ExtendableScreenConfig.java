package ch.mobi.mobitor.plugins.api.domain.config;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ExtendableScreenConfig {

    private static final int SIXTY_SECONDS = 60;

    @JsonProperty private String label;
    @JsonProperty private OnDutyConfig onDuty;
    @JsonProperty private List<String> environments;
    @JsonProperty private List<String> serverNames;
    @JsonProperty private int refreshInterval = SIXTY_SECONDS;

    @JsonIgnore private String configKey;

    @JsonIgnore private Map<String, JsonNode> pluginConfigs = new LinkedHashMap<>();
    @JsonIgnore private Map<String, List> pluginConfigObjsMap = new LinkedHashMap<>();

    @JsonIgnore private Set<String> retrievedPluginConfigProperties = new HashSet<>();

    public OnDutyConfig getOnDuty() {
        return onDuty;
    }

    public void setOnDuty(OnDutyConfig onDuty) {
        this.onDuty = onDuty;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getEnvironments() {
        return environments;
    }

    public void setEnvironments(List<String> environments) {
        this.environments = environments;
    }

    public List<String> getServerNames() {
        return serverNames;
    }

    public void setServerNames(List<String> serverNames) {
        this.serverNames = serverNames;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        this.refreshInterval = refreshInterval;
    }


    @JsonAnySetter
    public void setPluginConfig(String name, JsonNode node) {
        pluginConfigs.put(name, node);
    }

    public Map<String, JsonNode> getPluginConfigs() {
        return pluginConfigs;
    }

    @JsonIgnore
    public JsonNode getPluginConfigNode(String propertyName) {
        retrievedPluginConfigProperties.add(propertyName);
        return pluginConfigs.get(propertyName);
    }

    @JsonIgnore
    public void putPluginConfigList(String propName, List pluginConfigObjList) {
        this.pluginConfigObjsMap.put(propName, pluginConfigObjList);
    }

    @JsonIgnore
    public Map<String, List> getPluginConfigMap() {
        return pluginConfigObjsMap;
    }

    public Set<String> getUnreadPluginConfigProperties() {
        Set<String> pluginConfigPropertyNames = pluginConfigs.keySet();
        pluginConfigPropertyNames.removeAll(retrievedPluginConfigProperties);

        return pluginConfigPropertyNames;
    }

}
