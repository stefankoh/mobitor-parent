package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

/**
 * Used to evaluate the conformity of a pipeline.
 */
public enum RuleEvaluationSeverity {

    /**
     * The pipeline is "red" because there are rule violations.
     */
    CONTAINS_ERRORS,

    /**
     * The state of the pipeline is good enough.
     */
    WARNINGS_ONLY,

    /**
     * Everything is okay. No rule violations and no warnings.
     */
    TROPHY

}
